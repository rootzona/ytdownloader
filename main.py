import subprocess
import sys
from pytube import YouTube, Search
import customtkinter as ctk
from tkinter import *
import os
import getpass

ctk.set_appearance_mode("green")
ctk.set_default_color_theme("dark-blue")

app = ctk.CTk()
app.title("CatTube")
# app.geometry("1080x720")
# app.minsize(1080, 720)
# app.maxsize(1920, 1080)
# app.resizable(width=False, height=False)
ctk.CTkFont(family="rexlia")

img = 'img/ikona.png'
app.iconphoto(True, PhotoImage(file=os.path.join(img)))

ctk.set_appearance_mode("dark")
ctk.set_default_color_theme("blue")
# ctk.deactivate_automatic_dpi_awareness()
# ctk.set_widget_scaling(1)
# ctk.set_window_scaling(1)

var_success_mp4 = "MP4 Downloaded Successfully!"
var_success_mp3 = "MP3 Downloaded Successfully!"
var_file_exist = "File Already Exist"
var_wrong = "Something went wrong .."
var_ikona = "/ᐠ◕ᆽ◕ﾐᐟ\∫\nฅo ฅ"
var_ikona_middle = "\n^≚ﻌ≚^っ ..    0%\n"
var_ikona_middle_100 = "\n^≚ﻌ≚^っ ..    100%\n"
var = ctk.StringVar(app)
name = getpass.getuser()
music_path = os.path.join(f"/home/{name}/Music/")
# rexlia_font = os.path.join(os.path.dirname(__file__), 'font/rexlia_font.otf')


class App(ctk.CTk):
    def __init__(self, master, event):
        super().__init__()
        self.master = master
        width = event.width
        height = event.height
        app.configure(width=True, height=True)
#
# def resize_app(event, app):
#     width = event.width
#     height = event.height
#     app.configure(width=width, height=height)


def downloads(av):
    url = entry_url.get()
    yt = YouTube(url, on_progress_callback=on_progress)
    yt.streams.first()
    filename = os.path.basename(music_path)
    try:
        if check_file_exists(f"{music_path}{filename}"):
            # print(check_file_exists(f"{music_path}{filename}"))
            check_file_exists(status_lbl.configure(text=var_file_exist))
        # else:
        #     check_file_exists(status_lbl.configure(text=var_file_exist))
    except FileExistsError:
        return None

    try:
        if av == "MP3":
            stream = yt.streams.filter(only_audio=True, progressive=False).first()
            stream.download(filename=f"{music_path + yt.title}.mp3")
            status_lbl.configure(text=f"{yt.title} \n {var_success_mp3}", text_color="#a17e41",
                                 font=ctk.CTkFont(family="rexlia"))
            progress_bar.set(True)

        elif av == "MP4":
            stream = yt.streams.filter(progressive=False).first()
            stream.download(music_path)
            status_lbl.configure(text=f"{yt.title} \n {var_success_mp4}", text_color="#a17e41",
                                 font=ctk.CTkFont(family="rexlia"))
            progress_bar.set(True)
        elif av == "MP3" == "MP4":
            stream = yt.streams.filter(progressive=False).first()
            stream.download()
            if os.path.exists(filename):
                return "File already exists."
            else:
                label_text.configure(text=var_file_exist, font=ctk.CTkFont(family="rexlia"))
                print(var_file_exist)
                # return None
    except Exception as e:
        status_lbl.configure(text=f"Error (=◑ᆽ◑=)ฅ(ටᆽට=)\n {str(e)}", text_color="#A34124", static="nwse",
                             font=ctk.CTkFont(family="rexlia"))


def check_file_exists(filename):
    if os.path.exists(filename):
        label_text.configure(text=var_file_exist, font=ctk.CTkFont(family="rexlia", size=75))
        print(var_file_exist + " -> def check_file_exists")
    else:
        return None 


def btn_music_dir_open():
    opener = "open" if sys.platform == "darwin" else "xdg-open"
    subprocess.call([opener, music_path])


def change_theme_option(new_appearance_mode: str):
    ctk.set_appearance_mode(new_appearance_mode)


def btn_Clear():
    entry_url.delete(0, 'end')
    progress_lbl.configure(text=var_ikona_middle)
    progress_bar.set(0)
    status_lbl.configure(text="")
    label_text.configure(text="")


def exit_app():
    exit()


def on_progress(stream, chunk, bytes_remaining):
    ttl_size = stream.filesize
    bts_downloaded = ttl_size - bytes_remaining
    percentage_completed = bts_downloaded / ttl_size * 100

    progress_lbl.configure(text=f"\n^≚ﻌ≚^っ .. {str(int(percentage_completed)) + '%'}")
    progress_lbl.update()

    progress_bar.set(float(percentage_completed / 100))


def btn_none():
    try:
        url = entry_url.get()
        yt = YouTube(url, on_progress_callback=on_progress)

    except Exception as e:
        label_text.configure(text=f"Error (=◑ᆽ◑=)ฅ(ටᆽට=)\n {str(e)}", text_color="#A34124", static="nwse",
                             font=ctk.CTkFont(family="rexlia"))
    else:
        label_text.configure(
            text=f"Author:  {yt.author} \n\n Title:  {yt.title} \n\n Description:  {yt.description} \n\n "
                 f"Channel ID:  {yt.channel_id} \n\n Channel URL:  {yt.channel_url} \n\n"
                 f"Keywords:  {yt.keywords} \n\n Captions:  {yt.captions} \n\n "
                 f"Tracks Caption:  {yt.caption_tracks}")


# FRAME TOP LEFT #######################################################################################################
# ######################################################################################################################


LEFT_TOP_FRAME = ctk.CTkFrame(master=app)
LEFT_TOP_FRAME.grid(row=0, column=0, padx=0, pady=0, sticky='nwse')
LEFT_TOP_FRAME.configure()

left_label = ctk.CTkLabel(LEFT_TOP_FRAME, text=var_ikona, text_color="#ffffff")
left_label.grid(row=0, column=0, padx=28, pady=15, sticky='nwse')

# FRAME TOP RIGHT ######################################################################################################
# ######################################################################################################################


FRAME_TOP_RIGHT = ctk.CTkFrame(master=app)
FRAME_TOP_RIGHT.grid(row=0, column=1, padx=0, pady=0, sticky='nwse')

entry_url = ctk.CTkEntry(FRAME_TOP_RIGHT, width=745, height=40, placeholder_text="=^･ﻌ･^=っ..  PASTE URL HERE:",
                         placeholder_text_color="#F2FEFE", text_color="#F0FFF8", font=ctk.CTkFont(family="rexlia"),
                         border_width=3, border_color="#8CB1AA")
entry_url.grid(row=0, column=0, padx=0, pady=10, sticky="nwse")

formats = ["MP3", "MP4"]
segmented_button = ctk.CTkSegmentedButton(master=FRAME_TOP_RIGHT, values=formats,
                                          fg_color="#8CB1AA", font=ctk.CTkFont(family="rexlia"),
                                          selected_color="#000000", selected_hover_color="#111111",
                                          unselected_hover_color="#111111", text_color="white",
                                          command=downloads)
segmented_button.grid(row=0, column=1, padx=(5, 5), pady=10)
segmented_button.set("")
segmented_button.configure()

# FRAME LEFT MIDDLE ####################################################################################################
# ######################################################################################################################


FRAME_LEFT_MIDDLE = ctk.CTkFrame(master=app)
FRAME_LEFT_MIDDLE.grid(row=1, column=0, padx=0, pady=0, sticky="nwse")

top_clean = ctk.CTkButton(master=FRAME_LEFT_MIDDLE, fg_color="#111111", hover_color="#26282E",
                          text="CLEAR", command=btn_Clear, corner_radius=35, font=ctk.CTkFont(family="rexlia"))
top_clean.grid(row=0, column=0, padx=5, pady=5)
top_clean.configure(border_width=3, border_color="#8CB1AA")

top_open = ctk.CTkButton(master=FRAME_LEFT_MIDDLE, fg_color="#111111", hover_color="#26282E",
                         text="OPEN", command=btn_music_dir_open, corner_radius=35, font=ctk.CTkFont(family="rexlia"))
top_open.grid(row=1, column=0, padx=5, pady=0)
top_open.configure(border_width=3, border_color="#8CB1AA")

# label_bottom_left = ctk.CTkButton(master=FRAME_LEFT_MIDDLE, fg_color="#111111", text="ROOTZONA", corner_radius=35,
#                                   hover_color="#26282E", font=ctk.CTkFont(family="rexlia"))
# label_bottom_left.grid(row=2, column=0, sticky="nwse", padx=5, pady=5)
# label_bottom_left.configure(border_width=3, border_color="#8CB1AA")

themes = ["LIGHT", "DARK", "SYSTEM"]
theme_option_menu = ctk.CTkOptionMenu(master=FRAME_LEFT_MIDDLE, values=themes,
                                      command=change_theme_option, font=ctk.CTkFont(family="rexlia"),
                                      dropdown_font=ctk.CTkFont(family="rexlia"))
theme_option_menu.grid(row=2, column=0, padx=5, pady=(5, 0))
theme_option_menu.set("THEMES")
theme_option_menu.configure(button_color="#8CB1AA", fg_color="#111111", dropdown_fg_color="#838383",
                            dropdown_hover_color="#8CB1AA", button_hover_color="#26282E", corner_radius=35)

exit_button = ctk.CTkButton(master=FRAME_LEFT_MIDDLE, fg_color="#111111", hover_color="#26282E",
                            text="EXIT", command=exit_app, corner_radius=35, font=ctk.CTkFont(family="rexlia"))
exit_button.grid(row=3, column=0, padx=5, pady=5, sticky="nwse")
exit_button.configure(border_width=3, border_color="#8CB1AA")

# FRAME LEFT BOTTOM ####################################################################################################
########################################################################################################################

FRAME_LEFT_BOTTOM = ctk.CTkFrame(master=app)
FRAME_LEFT_BOTTOM.grid(row=2, column=0, padx=5, pady=5, sticky="nwse")

label_bottom_left = ctk.CTkLabel(master=FRAME_LEFT_BOTTOM, fg_color="#111111", text="DATA", corner_radius=35,
                                 font=ctk.CTkFont(family="rexlia"), anchor="center")
label_bottom_left.grid(row=0, column=0, sticky="nwse", padx=5, pady=5)

bottom_btn = ctk.CTkButton(master=FRAME_LEFT_BOTTOM, fg_color="#111111", hover_color="#26282E",
                           command=btn_none)
bottom_btn.grid(row=1, column=0, padx=5, pady=5, sticky="nwse")
bottom_btn.configure(text="DATA", font=ctk.CTkFont(family="rexlia"), text_color="white", border_width=3,
                     border_color="#8CB1AA", corner_radius=35)

# FRAME RIGHT MIDDLE ###################################################################################################
# ######################################################################################################################


FRAME_RIGHT_MIDDLE = ctk.CTkFrame(master=app)
FRAME_RIGHT_MIDDLE.grid(row=1, column=1, padx=0, pady=0, sticky='nwse')

# PROGRESS BAR FRAME MIDDLE ############################################################################################

progress_lbl = ctk.CTkLabel(FRAME_RIGHT_MIDDLE, text=var_ikona_middle, text_color="white",
                            corner_radius=15, anchor="center")
progress_lbl.grid(row=0, column=0, padx=5, pady=5)

progress_bar = ctk.CTkProgressBar(FRAME_RIGHT_MIDDLE, width=400, height=15, fg_color="#26282E")
progress_bar.grid(row=1, column=0, padx=200, pady=5)
progress_bar.get()
progress_bar.set(0)
progress_bar.configure(progress_color="#F2FEFE", border_width=3, border_color="#8CB1AA")

status_lbl_2 = ctk.CTkLabel(FRAME_RIGHT_MIDDLE, text="", width=150, height=75)
status_lbl_2.grid(row=2, column=0, padx=5, pady=5, sticky="nwse")
status_lbl_2.configure(set())

status_lbl = ctk.CTkLabel(FRAME_RIGHT_MIDDLE, text="", width=150, height=75)
status_lbl.grid(row=2, column=0, padx=5, pady=5, sticky="nwse")
status_lbl.configure(set())

# FRAME RIGHT BOTTOM ###################################################################################################
# ######################################################################################################################


FRAME_RIGHT_BOTTOM = ctk.CTkFrame(master=app)
FRAME_RIGHT_BOTTOM.grid(row=2, column=1, padx=0, pady=0, sticky='nwse')

# bottom_btn = ctk.CTkButton(master=FRAME_RIGHT_BOTTOM, fg_color="#111111", hover_color="#26282E",
#                            command=btn_none)
# bottom_btn.grid(row=0, column=0, padx=5, pady=5, sticky="w")
# bottom_btn.configure(text="DATA ABOUT VIDEO", font=ctk.CTkFont(family="rexlia"), text_color="white", border_width=3,
#                      border_color="#8CB1AA", corner_radius=35)

# scrolling_frame = ctk.CTkScrollbar(master=FRAME_RIGHT_BOTTOM)


label_text = ctk.CTkLabel(master=FRAME_RIGHT_BOTTOM, width=920, height=440, text="", fg_color="#26282E",
                          font=ctk.CTkFont(family="rexlia"))
label_text.grid(row=0, column=0, padx=5, pady=0)

if __name__ == "__main__":
    app.mainloop()
